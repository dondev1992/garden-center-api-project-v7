package io.catalyte.training.controllers;

import io.catalyte.training.entities.Customers;
import io.catalyte.training.services.CustomersServices;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/customers")
public class CustomersController {

    @Autowired
    private CustomersServices customersServices;

    @GetMapping
    public ResponseEntity<List<Customers>> getAllCustomers(Customers customer) {
        return new ResponseEntity<>(customersServices.getAllCustomers(customer), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Customers> getCustomerById(@PathVariable Long id) {
        return new ResponseEntity<>(customersServices.getCustomerById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Customers> addCustomer(@Valid @RequestBody Customers customer) {
        return new ResponseEntity<>(customersServices.addCustomer(customer), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Customers> updateCustomersById(
            @PathVariable Long id, @Valid @RequestBody Customers customer) {
        return new ResponseEntity<>(customersServices.updateCustomerById(id, customer), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable Long id) {
        customersServices.deleteCustomer(id);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllCustomers() {
        customersServices.deleteAllCustomers();
    }
}
