package io.catalyte.training.controllers;

import io.catalyte.training.entities.Users;
import io.catalyte.training.services.UsersServices;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class UsersController {

    @Autowired
    private UsersServices usersServices;

    @GetMapping
    public ResponseEntity <List<Users>> getAllUsers(Users user) {
        return new ResponseEntity<>(usersServices.getAllUsers(user), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Users> getUserById(@PathVariable Long id) {
        return new ResponseEntity<>(usersServices.getUserById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Users> addUser(@Valid @RequestBody Users user) {
        return new ResponseEntity<>(usersServices.addUser(user), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Users> updateUsersById(
            @PathVariable Long id, @Valid @RequestBody Users user) {
        return new ResponseEntity<>(usersServices.updateUserById(id, user), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        usersServices.deleteUser(id);
    }

//    @DeleteMapping
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteAllCustomers() {
//        usersServices.deleteAllUsers();
//    }


}
