package io.catalyte.training.controllers;

import io.catalyte.training.entities.Orders;
import io.catalyte.training.services.OrdersServices;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/orders")
public class OrdersController {

    @Autowired
    private OrdersServices ordersServices;

    List<Orders> ordersList = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<Orders>> getAllOrders(Orders orders) {
        return new ResponseEntity<>(ordersServices.getAllOrders(orders), HttpStatus.OK);
    }

//    @GetMapping
//    public ResponseEntity<List<Orders>> getAllOrders(@RequestParam(required = false) Date date, BigDecimal total, Products products, Customers customers, Integer quantity) {
//        return new ResponseEntity<>(ordersServices.FindByDateOrTotalOrProductsOrCustomersOrQuantity(date, total, products, customers, quantity), HttpStatus.OK);
//    }

//    @GetMapping(value = "/customerId")
//    public List<Orders> getOrdersByCustomersId(@Valid @RequestParam Long customers_id) {
//        ordersList = ordersServices.FindByCustomers_Id(customers_id);
//        return ordersList;
//    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Orders> getOrderById(@PathVariable Long id) {
        return new ResponseEntity<>(ordersServices.getOrderById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Orders> addOrder(@Valid @RequestBody Orders order) {
        return new ResponseEntity<>(ordersServices.addOrder(order), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Orders> updateOrdersById(
            @PathVariable Long id, @Valid @RequestBody Orders order) {
        return new ResponseEntity<>(ordersServices.updateOrderById(id, order), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrder(@PathVariable Long id) {
        ordersServices.deleteOrder(id);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllOrders() {
        ordersServices.deleteAllOrders();
    }
}


