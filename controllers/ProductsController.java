package io.catalyte.training.controllers;

import io.catalyte.training.entities.Products;
import io.catalyte.training.services.ProductsServices;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping(value = "/products")
public class ProductsController {

    @Autowired
    private ProductsServices productsServices;

    @GetMapping
    public ResponseEntity<List<Products>> getAllProducts(Products product) {
        return new ResponseEntity<>(productsServices.getAllProducts(product), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <Products> getProductById(@PathVariable Long id) {
        return new ResponseEntity<>(productsServices.getProductById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity <Products> addProduct(@Valid @RequestBody Products product) {
        return new ResponseEntity<>(productsServices.addProduct(product), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Products> updateProductsById(
            @PathVariable Long id, @Valid @RequestBody Products product) {
        return new ResponseEntity<>(productsServices.updateProductById(id, product), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long id) {
        productsServices.deleteProduct(id);
    }

//    @DeleteMapping
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteAllProducts() {
//        productsServices.deleteAllProducts();
//    }
}
