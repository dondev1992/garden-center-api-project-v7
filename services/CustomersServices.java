package io.catalyte.training.services;

import io.catalyte.training.entities.Customers;

import java.util.List;

public interface CustomersServices {

    List<Customers> getAllCustomers(Customers customer);

    Customers getCustomerById(Long id);

    Customers addCustomer(Customers customer);

    Customers updateCustomerById(Long id, Customers customer);

    void deleteCustomer(Long id);

    void deleteAllCustomers();
}
