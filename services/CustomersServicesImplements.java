package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.Conflict;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Customers;
import io.catalyte.training.repositories.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomersServicesImplements implements CustomersServices{

    @Autowired
    private CustomersRepository customersRepository;

    @Override
    public List<Customers> getAllCustomers(Customers customer) {
        try {
            if (customer.equals(null)) {
                return customersRepository.findAll();
            } else {
                Example<Customers> customerExample = Example.of(customer);
                return customersRepository.findAll(customerExample);
            }
        } catch ( Exception e) {
            throw new ServiceUnavailable(e);
        }
    }

    @Override
    public Customers getCustomerById(Long id) {
        try {
            Customers customer = customersRepository.findById(id).orElse(null);

            if (customer != null) {
                return customer;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the Customer
        throw new ResourceNotFound("Could not locate a Customer with the id: " + id);

    }

    @Override
    public Customers addCustomer(Customers customer) {
        if (!isParameterEmailEqualToCustomerEmail(customer)) {
            try {
                return customersRepository.save(customer);
            } catch (Exception e) {
                throw new ServiceUnavailable(e);
            }
        }
        throw new Conflict("Email address already exists");
    }

    @Override
    public Customers updateCustomerById(Long id, Customers customer) {
        if (!customer.getId().equals(id)) {
            throw new BadDataResponse("Customer id must match the id specified in the URL");
        }
        if (!isPathIdEqualToExistingCustomerId(customer)) {
            throw new BadDataResponse("Customer id does not exist");
        }
        List<Customers> customersList = customersRepository.findAll();
        for (Customers customersDb : customersList) {
            if (customersDb.getEmail().equals(customer.getEmail())) {
                if (!customersDb.getId().equals(customer.getId())) {
                    throw new Conflict("Email address already exists");
                }
            }

        }
        try {
            Customers customerFromDB = customersRepository.findById(id).orElse(null);

            if (customerFromDB != null) {
                return customersRepository.save(customer);
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
        // if we made it down to this pint, we did not find the Customer
        throw new ResourceNotFound("Could not locate a Customer with the id: " + id);
    }

    @Override
    public void deleteCustomer(Long id) {

        try {
            if (customersRepository.existsById(id)) {
                customersRepository.deleteById(id);
                return;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the Customer
        throw new ResourceNotFound("Could not locate a Customer with the id: " + id);
    }

    @Override
    public void deleteAllCustomers() {
                customersRepository.deleteAll();
    }

    public boolean isParameterEmailEqualToCustomerEmail(Customers customer) {
        List<Customers> customersList = customersRepository.findAll();
        for (Customers customersDb : customersList) {
            if (customersDb.getEmail().equals(customer.getEmail())) {
                return true;
            }
        }
        return false;
    }

    public boolean isPathIdEqualToExistingCustomerId(Customers customer) {
        List<Customers> customersList = customersRepository.findAll();
        for (Customers customersDb : customersList) {
            if (customersDb.getId().equals(customer.getId())) {
                return true;
            }
        }
        return false;
    }
}
