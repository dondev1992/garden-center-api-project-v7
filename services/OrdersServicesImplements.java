package io.catalyte.training.services;

import io.catalyte.training.customExceptions.BadDataResponse;
import io.catalyte.training.customExceptions.ResourceNotFound;
import io.catalyte.training.customExceptions.ServiceUnavailable;
import io.catalyte.training.entities.Orders;
import io.catalyte.training.repositories.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrdersServicesImplements implements OrdersServices{

    @Autowired
    private OrdersRepository ordersRepository;

    List<Orders> ordersList = new ArrayList<>();

    @Override
    public List<Orders> getAllOrders(Orders Orders) {
        try {
            if (Orders.isEmpty()) {
                return ordersRepository.findAll();
            } else {
                Example<Orders> orderExample = Example.of(Orders);
                return ordersRepository.findAll(orderExample);
            }
        } catch ( Exception e) {
            throw new ServiceUnavailable(e);
        }
    }

    @Override
    public Orders getOrderById(Long id) {
        try {
            Orders order = ordersRepository.findById(id).orElse(null);

            if (order != null) {
                return order;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the Order
        throw new ResourceNotFound("Could not locate a Order with the id: " + id);

    }

    @Override
    public Orders addOrder(Orders order) {
        try {
            return ordersRepository.save(order);
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
    }

    @Override
    public Orders updateOrderById(Long id, Orders order) {
        if (!order.getId().equals(id)) {
            throw new BadDataResponse("Order id must match the id specified in the URL");
        }
        try {
            Orders orderFromDB = ordersRepository.findById(id).orElse(null);

            if (orderFromDB != null) {
                return ordersRepository.save(order);
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
        // if we made it down to this pint, we did not find the Order
        throw new ResourceNotFound("Could not locate a Order with the id: " + id);
    }

    @Override
    public void deleteOrder(Long id) {

        try {
            if (ordersRepository.existsById(id)) {
                ordersRepository.deleteById(id);
                return;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }

        // if we made it down to this pint, we did not find the Order
        throw new ResourceNotFound("Could not locate a Order with the id: " + id);
    }

    @Override
    public void deleteAllOrders() {
        ordersRepository.deleteAll();
    }

}
