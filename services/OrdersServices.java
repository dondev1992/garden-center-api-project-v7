package io.catalyte.training.services;

import io.catalyte.training.entities.Orders;

import java.util.List;

public interface OrdersServices {

    List<Orders> getAllOrders(Orders Orders);

    Orders getOrderById(Long id);

    Orders addOrder(Orders order);

    Orders updateOrderById(Long id, Orders order);

    void deleteOrder(Long id);

    void deleteAllOrders();

//    List<Orders> FindByCustomers_Id(Long customers_id);

}
