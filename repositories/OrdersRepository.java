package io.catalyte.training.repositories;

import io.catalyte.training.entities.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {

    List<Orders> findByCustomers_Id(Long customers_id);

//    List<Orders> findByDateOrTotalOrProductsOrCustomersOrQuantity(Date date, BigDecimal total, Products products, Customers customers, Integer quantity);
}
