package io.catalyte.training.customExceptions;

import org.springframework.http.HttpStatus;

public class Conflict extends RuntimeException {

    public Conflict() {
    }

    public Conflict(String message) {
        super(message);
    }
}

