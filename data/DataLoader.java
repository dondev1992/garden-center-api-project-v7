package io.catalyte.training.data;


import io.catalyte.training.entities.*;
import io.catalyte.training.repositories.CustomersRepository;
import io.catalyte.training.repositories.OrdersRepository;
import io.catalyte.training.repositories.ProductsRepository;
import io.catalyte.training.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private ProductsRepository productsRepository;
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private UsersRepository usersRepository;

    private Customers customer1;
    private Customers customer2;
    private Customers customer3;
    private Customers customer4;
    private Customers customer5;
    private Customers customer6;

    private Products product1;
    private Products product2;
    private Products product3;
    private Products product4;
    private Products product5;
    private Products product6;

    private Users user1;
    private Users user2;
    private Users user3;

    private Orders order1;
    private Orders order2;
    private Orders order3;
    private Orders order4;
    private Orders order5;
    private Orders order6;
    private Orders order7;
    private Orders order8;
    private Orders order9;
    private Orders order10;
    private Orders order11;
    private Orders order12;



    @Override
    public void run(String... args) throws Exception {
        loadUsers();
        loadCustomers();
        loadProducts();
        loadOrders();

    }

    private void loadProducts() {
        product1 = productsRepository.save(new Products(495837, "safety", "gloves", "Bully Tools", new BigDecimal("12.49")));
        product2 = productsRepository.save(new Products(499432, "tool", "shears", "Lowes", new BigDecimal("17.99")));
        product3 = productsRepository.save(new Products(496330, "tool", "loppers", "Stoller", new BigDecimal("26.99")));
        product4 = productsRepository.save(new Products(497432,"tool", "fork", "Bully Tools", new BigDecimal("22.79")));
        product5 = productsRepository.save(new Products(498835, "tool", "spade", "Stoller", new BigDecimal("12.49")));
        product6 = productsRepository.save(new Products(497212, "cleanup", "lawn bags", "Lowes", new BigDecimal("15.59")));
    }

    private void loadCustomers() {
        customer1 = customersRepository.save(new Customers("Churina", "churina@email.com", new Address("123 Tryon St.",  "Charlotte", "NC", 28215)));
        customer2 = customersRepository.save(new Customers("Will", "will@email.com", new Address("4652 Brevard St.", "Charlotte", "NC", 28202)));
        customer3 = customersRepository.save(new Customers("Jazmarie", "jazmarie@email.com", new Address("2023 Trade St.", "Charlotte", "NC", 28269)));
        customer4 = customersRepository.save(new Customers("Richy", "richy@email.com", new Address("3745 Independence Blvd.", "Matthews", "NC", 28227)));
        customer5 = customersRepository.save(new Customers("DI", "di@email.com", new Address("987 Graham St.", "Charlotte", "NC", 28364)));
        customer6 = customersRepository.save(new Customers("Jared", "jared@email.com", new Address("110 South Blvd.", "Charlotte", "NC", 28215)));
    }

    private void loadUsers() {
        user1 = usersRepository.save(new Users("Rachel", "Cashier", List.of("EMPLOYEE"), "rachel@email.com", "happyDays2023"));
        user2 = usersRepository.save(new Users("Michael", "Lead Programmer", List.of("EMPLOYEE", "ADMIN"), "michael@email.com", "liveLife09"));
        user3 = usersRepository.save(new Users("Kevin", "Manager", List.of("EMPLOYEE", "ADMIN"), "kevin@email.com", "28DaysOfNight"));
    }

    private void loadOrders() {
        Items item1 = new Items(product6, 4);
        Items item2 = new Items(product5, 2);
        Items item3 = new Items(product4, 8);
        Items item4 = new Items(product3, 12);
        Items item5 = new Items(product2, 1);
        Items item6 = new Items(product1, 5);
        Items item7 = new Items(product6, 4);
        Items item8 = new Items(product5, 2);
        Items item9 = new Items(product4, 8);
        Items item10 = new Items(product3, 12);
        Items item11 = new Items(product2, 1);
        Items item12 = new Items(product1, 5);
        order1 = ordersRepository.save(new Orders(customer1, Date.from(
                LocalDate.parse("2022-10-17").atStartOfDay(ZoneId.systemDefault()).toInstant()), item1, new BigDecimal("62.40")
        ));
        order2 = ordersRepository.save(new Orders(customer2, Date.from(
                LocalDate.parse("2022-12-13").atStartOfDay(ZoneId.systemDefault()).toInstant()), item2, new BigDecimal("24.98")
        ));
        order3 = ordersRepository.save(new Orders(customer3, Date.from(
                LocalDate.parse("2022-12-15").atStartOfDay(ZoneId.systemDefault()).toInstant()), item3, new BigDecimal("182.32")
        ));
        order4 = ordersRepository.save(new Orders(customer4, Date.from(
                LocalDate.parse("2022-10-17").atStartOfDay(ZoneId.systemDefault()).toInstant()), item4, new BigDecimal("323.88")
        ));
        order5 = ordersRepository.save(new Orders(customer5, Date.from(
                LocalDate.parse("2022-11-06").atStartOfDay(ZoneId.systemDefault()).toInstant()), item5, new BigDecimal("17.990")
        ));
        order6 = ordersRepository.save(new Orders(customer6, Date.from(
                LocalDate.parse("2022-09-18").atStartOfDay(ZoneId.systemDefault()).toInstant()), item6, new BigDecimal("62.45")
        ));
        order7 = ordersRepository.save(new Orders(customer4, Date.from(
                LocalDate.parse("2021-12-22").atStartOfDay(ZoneId.systemDefault()).toInstant()), item7, new BigDecimal("74.94")
        ));
        order8 = ordersRepository.save(new Orders(customer4, Date.from(
                LocalDate.parse("2021-12-31").atStartOfDay(ZoneId.systemDefault()).toInstant()), item8, new BigDecimal("237.31")
        ));
        order9 = ordersRepository.save(new Orders(customer1, Date.from(
                LocalDate.parse("2022-10-27").atStartOfDay(ZoneId.systemDefault()).toInstant()), item9, new BigDecimal("89.95")
        ));
        order10 = ordersRepository.save(new Orders(customer2, Date.from(
                LocalDate.parse("2022-11-11").atStartOfDay(ZoneId.systemDefault()).toInstant()), item10, new BigDecimal("161.94")
        ));
        order11 = ordersRepository.save(new Orders(customer4, Date.from(
                LocalDate.parse("2023-01-23").atStartOfDay(ZoneId.systemDefault()).toInstant()), item11, new BigDecimal("24.98")
        ));
        order12 = ordersRepository.save(new Orders(customer6, Date.from(
                LocalDate.parse("2023-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()), item12, new BigDecimal("49.96")
        ));
    }
}
