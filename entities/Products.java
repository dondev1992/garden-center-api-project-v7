package io.catalyte.training.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Digits;

import java.math.BigDecimal;

@Entity
@Table(name = "products")
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer sku;
    private String type;
    private String name;
    private String manufacturer;
    @Digits(integer = 2, fraction = 2, message = "Prices must show 2 decimals spaces to the right")
    private BigDecimal price;
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "productId", referencedColumnName = "id")
    private Items items;

    public Products() {
    }

//    public Products(Integer sku, String type, String name, String manufacturer, BigDecimal price, Items items) {
//        this.sku = sku;
//        this.type = type;
//        this.name = name;
//        this.manufacturer = manufacturer;
//        this.price = price;
//        this.items = items;
//    }

    public Products(Integer sku, String type, String name, String manufacturer, BigDecimal price) {
        this.sku = sku;
        this.type = type;
        this.name = name;
        this.manufacturer = manufacturer;
        this.price = price;
    }

    public Products(Long id, Integer sku, String type, String name, String manufacturer, BigDecimal price) {
        this.id = id;
        this.sku = sku;
        this.type = type;
        this.name = name;
        this.manufacturer = manufacturer;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSku() {
        return sku;
    }

    public void setSku(Integer sku) {
        this.sku = sku;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", sku=" + sku +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                "" +
//                ", items=" + items +
                '}';
    }
}
