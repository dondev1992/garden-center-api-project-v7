package io.catalyte.training.entities;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

@Entity
public class Items {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @Valid
    @JoinColumn(name = "productId")
    private Products products;

    @Min(value = 1)
    private Integer quantity;

    @OneToOne
    private Orders orders;

    public Items() {
    }

//    public Items(Products products, Orders orders, Integer quantity) {
//        this.products = products;
//        this.orders = orders;
//        this.quantity = quantity;
//    }

    public Items(Products products, Integer quantity) {
        this.products = products;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Items{" +
                "id=" + id +
                ", products=" + products +
                ", quantity=" + quantity +
                '}';
    }
}
